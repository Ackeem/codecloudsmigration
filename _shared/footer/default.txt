<style media="screen">
.footer_nav{
    list-style:none;
    margin:0;
    padding:0;
    text-align:center;
    width: auto!important;
}
.footer_nav li{
    display:inline;
}
.footer_nav a{
    display:inline-block;
    padding:10px;
}

#footer a,
#footer a:link,
#footer a:visited,
#footer a:hover,
#footer a:active {
color:black;
text-decoration:none;
font-size: 12px;
}

#footer{
  font-size: 12px;
}

.white-popup {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
    font-weight: normal;
	position: relative;
	background: #FFF;
    color: #000;
	padding: 20px;
	width: auto;
	max-width: 860px;
	margin: 20px auto;
	border-radius: 5px;
    font-weight: normal;
}
.white-popup.ic-loading {
    max-width: 300px;
    text-align: center;
    font-size: 1em;
    font-weight: bold;
}
.white-popup .ic-modal-title {
    border-bottom: 1px solid #ccc;
    font-size: 1.2em;
    font-weight: bold;
    padding-bottom: 5px;
}
.white-popup .ic-modal-content {

}
.white-popup .ic-modal-content ul {
    padding: 0 0 0 40px;
    margin: 0;
}
.white-popup .ic-modal-content p {
    margin: 10px 0;
}
.content.white-popup p {
	text-align: justify;
}
.content.white-popup h1 {
	text-align: center;
	color: #333;
	font-size: 1.5em;
    margin: 0.5 em 0;
	font-weight: bold;
	text-transform: uppercase;
}
.content.white-popup h2 {
    font-size: 1.3em;
    margin: 0.5 em 0;
}
.ic-side-by-side {
    text-align: center;
}
.ic-side-by-side button {
    display: inline;
    margin: 0 5px;
}
</style>
<div id="footer">

<?php

include($_SERVER['DOCUMENT_ROOT'].'/settings/update/FAKE_config.php');

//CHECK FOR CONFIG FILE FIRST
if (isset($study)){ $_study=$study; }
if (isset($contact)){ $_contact=$contact; }
if (isset($privacy)){ $_privacy=$privacy; }
if (isset($term)){ $_term=$term; }

//IF NOT USE DEFAULT FOR BRAND ON THE SETTINGS FILE
if (  (!isset($study)) or (!isset($contact)) or (!isset($privacy)) or (!isset($term)) ){
    include($_SERVER['DOCUMENT_ROOT'].'/settings/update/settings.php');
    if (!isset($study)){ $_study=$study; }
    if (!isset($contact)){ $_contact=$contact; }
    if (!isset($privacy)){ $_privacy=$privacy; }
    if (!isset($term)){ $_term=$term; }
}

 include('study_'.$_study.'.php'); ?>

<ul class="footer_nav">
  <li><a class="modalClick" onClick="isExit=false;" href="<?php echo 'https://'.$_SERVER['SERVER_NAME'].'/settings/update/footer/contact_'.$_contact.'.php?active='.$_SESSION["active_prod_id"]; ?>">Contact</a></li>
  <li><a class="modalClick" onClick="isExit=false;" href="<?php echo 'https://'.$_SERVER['SERVER_NAME'].'/settings/update/footer/privacy_'.$_privacy.'.php?active='.$_SESSION["active_prod_id"]; ?>">Privacy Policy</a></li>
  <li><a class="modalClick" onClick="isExit=false;" href="<?php echo  'https://'.$_SERVER['SERVER_NAME'].'/settings/update/footer/term_'.$_term.'.php?active='.$_SESSION["active_prod_id"]; ?>">Terms and Conditions</a></li>
</ul>
</div>

<script type="text/javascript">
  function modalOnClick() {
    $('.modalClick').each(function() {
      var url = $(this).attr('href');
      $(this).magnificPopup({
        closeOnContentClick: false,
        type: 'ajax',
        ajax: {
          settings: null,
          cursor: 'mfp-ajax-cur', // CSS class that will be added to body during the loading (adds "progress" cursor)
          tError: '<a href="%url%">The content</a> could not be loaded.' //  Error message, can contain %curr% and %total% tags if gallery is enabled
        },
        callbacks: {
          parseAjax: function(mfpResponse) {
            mfpResponse.data = $(mfpResponse.data).closest('.content');
          },
          ajaxContentAdded: function() {
            modalOnClick();
          }
        }
      });

    }).click(function(event) {
      event.preventDefault();
    });
  }

  $(window).load(function(){
    modalOnClick();
  });
  </script>
