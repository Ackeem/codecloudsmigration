<!DOCTYPE html>
<html>
<head>
      <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="assets/css/thankyou.min.css">
    
</head>
<body>
    
    <div id="outer-wrapper" class = "after">
        <div id="wrapper" class = "after">
            <div id="banner">
                <img src="https://images.templatemgr.com/brands/81/generic/step1/logo_400x200.png" height="80" width="80" alt="logo" class="logo"/>
                <div id="steps">
                    <ul>
                        <li class="step-1">
                            <div class="step">
                                <p>1</p>
                            </div>
                            <p class="step-name">SHIPPING INFO</p>
                        </li>
                        <li class="step-2">
                            <div class="step">
                                <p>2</p>
                            </div>
                            <p class="step-name">FINISH ORDER</p>
                        </li>
                        <li class="step-3 thank-you">
                            <div class="step">
                                <p><img src="https://images.templatemgr.com/templates/v23/img/step-check.png" alt="check" class="check"/></p>
                            </div>
                            <p class="step-name">SUMMARY</p>
                        </li>
                    </ul>
                    <div id="steps line">
                        <div id="ty-slider"></div>
                    </div>
                </div>
            </div>
            <div id="thank-you">
                <div id="ty-msg" class="grey-2">
                    <p class="bl f18">THANK YOU FOR YOUR PURCHASE</p>
                    <p class="f14">We hope you enjoy the benefits of ##brand## #product## </p>
                    <p class="f14">Your order is scheduled to arrive by <span class="red bl"><?php echo date("F j, Y", strtotime("+5 days")); ?></span></p>
                </div>
                <img src="https://images.templatemgr.com/brands/81/generic/step1/step1_400x400.png" id="prod" alt="product"/>
                <div id = "billing" class="grey-2" >
                    <h1 class="bl">ORDER RECEIPT</h1>
                    <div id = "billing-inner">
                        <div id="order-info">
                            <p class = "pb5"><span class = "bl">Order Placed:</span> <?php echo date("F j, Y"); ?></p>
                            <p><span class = "bl">Order Number:</span> ##orderid##</p>
                            <p class="bl">Items Ordered:</p>
                        </div>
                        <div class="after bb pb10" >
                            <div ng-repeat="info in prodInfo.productContent">
                                <p class = "bl pb5">##brand##</p>
                                <p class="pb5">##product_name##</p>
                                <p class="fl">30ml - 30 Day Supply</p>
                                <p class="green bl fr f18">$##price##</p>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div style="width: 100%; height:100px;"></div>
            </div>
            <div id="line"></div>
        </div>
    </div>
    <script src="assets/js/action.min.js"></script>
</body>
</html>
