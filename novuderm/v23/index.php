<!DOCTYPE html>
    <head class="ng-scope" ng-controller="ListCtrl">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="stylesheet" type="text/css" href="assets/css/index.min.css" />
    </head>
    <body>
        
        <div id="repeat-bg">
            <div id="outer-wrapper">
                <div id="warning">
                    <p><span class="warning-word black">WARNING:</span> Due to high demand from recent media coverage we can no longer guarantee supply.<br> As of <?php echo date("F d, Y"); ?> we currently have product in-stock and will ship within 24 hours of purchase.</p>
                </div>
                <div id="top" class="after" data-type="background" data-speed="1.2">
                    <div id="top-inner" class="after">
                        <div id="form-container">
                            <div id="form">
                                <div class="timer limited-supply"><span>2</span><span>5</span><span>0</span></div>
                                <div id="form-top">
                                </div>
                                <form class="ng-pristine ng-valid" action="" method="POST" id="opt_in_form" name="customer_form" novalidate>
                                    <div id="form-inner">
                                        <!-- the form elements -->
                                    </div>
                                    <div id="form-btm">
                                        <div id="lock"></div>
                                        <input onclick="set_exit_var()" id="rushMyTrailBtn" type="submit" value="RUSH MY TRIAL" />
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div style="bottom: -8px;" id="reminder">
                            <p style="opacity: 1;" class="mb10"><span class="bl">LIMIT 1 TRIAL PER CUSTOMER</span><br>Don’t get left behind! </p>
                        </div>
                        <img id="brandImage1" src="img/logo_400x200.png" alt="##brandName## ##productName##" />
                        <img id="productImage1" src="img/step1_400x400.png" alt="##brandName## ##productName##" />
                    </div>
                </div>
                <div id="wrapper">
                    <div class="ng-scope" ng-controller="headerDir">
                        <div class="ng-scope" ng-include="template.url" ng-controller="ListCtrl">
                            <div class="ng-scope" ng-repeat="content in heading">
                                <div id="section-1" class="after">
                                    <h2><span>Featured:</span> The Science</h2>
                                    <div class="content1">
                                        <p>Seventy five percent of our skin is comprised of water and collagen. Our skin is exposed to harsh UVA and UVB radiation resulting in age spots, fine lines, and wrinkles. As we age, our bodies produce less and less collagen, leading to the formation of wrinkles and fine lines.  Most ##productName## products use fragments of hydrolyzed collagen containing molecules too large for the skin with conventional formulas. ##brandName##’s breakthrough formula delivers whole collagen molecules to the skin. The peptide-rich wrinkle serum is applied to the skin, rebuilding and rejuvenating the skin.</p>
                                    </div>
                                </div>
                                <div id="section-2" class="after">
                                    <div class="content-list">
                                        <img src="https://images.templatemgr.com/templates/v23/img/spot-header.png" width="331" height="89" alt="In the Spotlight" class="spot-header">
                                        <div class="headers">
                                            <h2>BENEFITS OF ##brandName##</h2>
                                            <h3>##productName## Formula</h3>
                                        </div>
                                        <ul>
                                            <li class="ng-scope" ng-repeat="listData in content.sec1List">
                                                <div class="content">
                                                    <div class="line"></div>
                                                    <h3 class="ng-binding">Eliminates the Look of Dark Circles</h3>
                                                    <p class="ng-binding">Restores nourishment in form of hydration to the under-eye area removing puffiness.</p>
                                                </div>
                                            </li>
                                            <li class="ng-scope" ng-repeat="listData in content.sec1List">
                                                <div class="content">
                                                    <div class="line"></div>
                                                    <h3 class="ng-binding">Reduces the Appearance of Wrinkles</h3>
                                                    <p class="ng-binding">The boost in collagen and elastin helps retain the skin's dermal structure which results in reduction of the look of fine lines.</p>
                                                </div>
                                            </li>
                                            <li class="ng-scope" ng-repeat="listData in content.sec1List">
                                                <div class="content">
                                                    <div class="line"></div>
                                                    <h3 class="ng-binding">Enhances Skin Hydration</h3>
                                                    <p class="ng-binding">Active Ingredients facilitate in trapping moisture, which in turn hydrates the skin and prevents cracking.</p>
                                                </div>
                                            </li>
                                            <li class="ng-scope" ng-repeat="listData in content.sec1List">
                                                <div class="content">
                                                    <div class="line"></div>
                                                    <h3 class="ng-binding">Counters Effects of Stress</h3>
                                                    <p class="ng-binding">Boosts skin immunity and prevents damaging effects of free radicals. Eliminates debris that makes skin dull and discolored.</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="cta">
                                        <div class="content">
                                            <h2>ACHIEVE VISIBLY YOUNGER LOOKING SKIN!</h2>
                                            <p>##brandName## Supplies are limited. Get it today!</p>
                                        </div>
                                        <a href="#" onclick="isExit=false;" ><img src="https://images.templatemgr.com/templates/v23/img/rush-arrow.png" width="11" height="13" alt="arrow" class="arrow">RUSH MY TRIAL</a>
                                    </div>
                                </div>
                                <div id="section-3" class="after">
                                    <h2><span>Exclusive</span> -  Achieve Healthy, Younger Looking Skin</h2>
                                    <div class="content">
                                        <ul>
                                            <li><p>NO INVASIVE SURGERY</p></li>
                                            <li><p>NO PAINFUL INJECTIONS</p></li>
                                            <li><p>NO EXPENSIVE LASER</p></li>
                                        </ul>
                                        <p class="ng-binding ng-scope" ng-repeat="contentData in content.sec3content">Your skin is the largest organ on your body but one of the most exposed. As a child, your skin is smooth, healthy and vibrant. But as we age, it can become wrinkled, saggy and discolored. Why? Our skin dries and loses its elasticity as it is exposed to a host of factors which attack your skin including age, wind, dryness, sun damage and free radicals.</p><!-- end ngRepeat: contentData in content.sec3content --><p class="ng-binding ng-scope" ng-repeat="contentData in content.sec3content">Each of these factors reduce our skin’s ability to keep its elasticity and firmness. Age also leads to the loss and breakdown of collagen in the dermal layer of your skin. Collagen is key to provide structural support to your skin. As our body’s produce less, your skin begins to sag and wrinkle.</p><!-- end ngRepeat: contentData in content.sec3content --><p class="ng-binding ng-scope" ng-repeat="contentData in content.sec3content">Your best defense against these hostile factors is choosing the right skin care product that may help reduce visible signs of aging. ##brandName## is just the solution!</p><!-- end ngRepeat: contentData in content.sec3content -->
                                    </div>
                                    <div>
                                        <img src="https://images.templatemgr.com/templates/v23/img/sec-3-jar-container.png" width="519" height="557" alt="Product Jar" class="jar" />
                                        <img id="productImage2" src="img/step1_200x200.png" alt="##brandName## ##productName##" />
                                    </div>
                                </div>
                                <div id="section-4" class="after">
                                    <img src="https://images.templatemgr.com/templates/v23/img/girls.png" width="503" height="505" alt="">
                                    <div id="section4Box">
                                        <div id="section4Text1">##brandName##</div>
                                        <div id="section4Text2"># ##brandName##<br><span style="font-size:15px;font-weight:normal;">IT'S UNANIMOUS!</span></div>
                                    </div>
                                    <div class="content content-4">
                                        <h2><span>Spotlight:</span> What’s all the buzz about?</h2>
                                        <div class="content-inner">
                                            <h3>The New Injection-Free Solution</h3>
                                            <p>Ever wonder how celebrities keep their skin looking so flawless and wrinkle-free?  For porcelain looking skin, celebrities look for ##productName## creams which give them the appearance of smooth, supple, wrinkle-free skin. </p>
                                            <p>##brandName## is your secret to radiant, beautiful skin that looks years younger. Don’t endure the physical pain and expense of costly procedures and surgeries. ##brandName## works naturally to help replenish your skin’s moisture, firming its appearance and restoring your natural glow to reveal a younger-looking you.</p>
                                            <img src="https://images.templatemgr.com/templates/v23/img/quote-1.jpg" width="281" height="50" alt="quote1" class="quote">
                                            <img src="https://images.templatemgr.com/templates/v23/img/quote-2.jpg" width="326" height="50" alt="quote2" class="quote">
                                        </div>
                                    </div>
                                    <div class="cta">
                                        <div class="content">
                                            <h2>ACHIEVE VISIBLY YOUNGER LOOKING SKIN!</h2>
                                            <p>##brandName## Supplies are limited. Get it today!</p>
                                        </div>
                                        <a href="#" onclick="isExit=false;"><img src="https://images.templatemgr.com/templates/v23/img/rush-arrow.png" width="11" height="13" alt="arrow" class="arrow">RUSH MY TRIAL</a>
                                    </div>
                                </div>
                                <div id="section-5">
                                    <div class="content">
                                        <img src="https://images.templatemgr.com/templates/v23/img/ing-1.jpg" width="321" height="304" alt="vitalize" />
                                        <img src="https://images.templatemgr.com/templates/v23/img/ing-2.jpg" width="321" height="303" alt="replenish" />
                                        <img src="https://images.templatemgr.com/templates/v23/img/ing-3.jpg" width="321" height="302" alt="moisturize" />
                                    </div>
                                    <img id="productImage4" src="img/logo_400x200.png" alt="##brandName## ##productName##" />
                                    <img id="productImage3a" src="img/step1_400x400.png" alt="##brandName## ##productName##" />
                                </div>
                                <div id="btm">
                                    <div class="cta">
                                        <a href="#" onclick="isExit=false;" style="right: 8px;top: 48px;"><img src="https://images.templatemgr.com/templates/v23/img/rush-arrow.png" width="11" height="13" alt="arrow" class="arrow">RUSH MY TRIAL</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="popover">
                    <div style="top: 0px; opacity: 1;" id="pop-box">
                        <div>
                            <h2>WAIT!</h2>
                            <div class="para">
                                <h3>Are you sure you want to leave?</h3>
                                <p><span class="f34 grey">DON'T MISS OUT ON THIS EXCLUSIVE OFFER</span></p>
                                <img src="https://images.templatemgr.com/templates/v23/img/down-prod.png" width="748" height="264" alt="down-prod" />
                                <img id="popupImg1" src="img/step1_400x400.png" alt="product" />
                            </div>
                        </div>
                        <p><a href="order.php?exitpop=true" onclick="isExit=false;" id="down-btn">CLAIM YOUR EXCLUSIVE TRIAL</a></p>
                        <div id="pop-terms">
                            <p>We are confident that ##brandName## is your new injection-free solution to beautiful, radiant-looking skin. Packed with powerful ##productName## ingredients, ##brandName## works to eliminate the look of dark circles, reduce the appearance of wrinkles, enhance skin hydration, and counter effects of stress. Say no to needles - achieve visibly younger looking skin the natural way! Claim your exclusive trial of ##brandName## now and start seeing results.<br><a href="#" onclick="isExit=false;removePopup();">No thanks, I’d rather not have beautiful, radiant-looking skin.</a></p>
                        </div>
                    </div>
                </div>
                <div id="btn">
                    <img src="https://images.templatemgr.com/templates/v23/img/arrow.png" width="24" height="16" alt="arrow" />
                    <div id="btn-overlay"></div>
                </div>
            </div>
        </div>
        <div style="display: none;" id="cboxOverlay"></div>
        <div style="display: none;" tabindex="-1" role="dialog" class="" id="colorbox"><div id="cboxWrapper"><div><div style="float: left;" id="cboxTopLeft"></div><div style="float: left;" id="cboxTopCenter"></div><div style="float: left;" id="cboxTopRight"></div></div><div style="clear: left;"><div style="float: left;" id="cboxMiddleLeft"></div><div style="float: left;" id="cboxContent"><div style="float: left;" id="cboxTitle"></div><div style="float: left;" id="cboxCurrent"></div><button id="cboxPrevious" type="button"></button><button id="cboxNext" type="button"></button><button id="cboxSlideshow"></button><div style="float: left;" id="cboxLoadingOverlay"></div><div style="float: left;" id="cboxLoadingGraphic"></div></div><div style="float: left;" id="cboxMiddleRight"></div></div><div style="clear: left;"><div style="float: left;" id="cboxBottomLeft"></div><div style="float: left;" id="cboxBottomCenter"></div><div style="float: left;" id="cboxBottomRight"></div></div></div><div style="position: absolute; width: 9999px; visibility: hidden; display: none; max-width: none;"></div></div>
        
        <script type="text/javascript" src="assets/js/index.min.js"></script>
        <script type="text/javascript" src="assets/js/popup.min.js"></script>
    </body>
</html>
