
<!DOCTYPE html>
<head class="ng-scope" ng-controller="ListCtrl">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" href="assets/css/order.min.css" />
</head>
<body>

    <div id="outer-wrapper">
        <div id="wrapper" class="after">
            <div id="banner">
                <img id="brandImg" src="img/logo_400x200.png" alt="logo" class="logo" />
                <div id="steps">
                    <ul>
                        <li class="step-1">
                            <div class="step">
                                <p>1</p>
                            </div>
                            <p class="step-name">SHIPPING INFO</p>
                        </li>
                        <li class="step-2">
                            <div style="background: rgb(1, 170, 201) none repeat scroll 0% 0%;" class="step">
                                <p>2</p>
                            </div>
                            <p class="step-name">FINISH ORDER</p>
                        </li>
                        <li class="step-3">
                            <div class="step">
                                <p><img src="img/order/step-check.png" alt="check" class="check step-check" /></p>
                            </div>
                            <p class="step-name">SUMMARY</p>
                        </li>
                    </ul>
                    <div id="steps line">
                        <div style="width: 143px;" id="slider"></div>
                    </div>
                </div>
                <div class="demographic">
                    <img src="img/order/flag.png" alt="flag" class="flag usa-flag" />
                    <p>Internet Exclusive Offers<br />Available to US Residents Only</p>
                </div>
            </div>

            <div id="approve-container">
                <div style="opacity: 1;" id="viewing">
                    <img src="img/order/eye.png" alt="eye" class="eye" />
                    <p>13 others are viewing this offer right now - <span id="countdown" class="bl">05:00</span></p>
                </div>
                <p class="f17 bl grey-2 pb15"><span class="green"><span class="green">Great Job ##first_name## ##last_name## !</span>  You’re taking your first step towards better skin. Act now so you don’t miss out on this offer!</span></p>
                <p class="f14 grey-2 pb15">Current Availability: <span class="low-avail availability"><span class="red"></span><span></span><span></span><span></span><span></span></span><span class="bl">LOW STOCK.</span> Sell-out Risk: <span class="red bl">HIGH</span> </p>
                <p class="f14 grey-2 pb5">Just pay for shipping and handling. Enjoy your expedited delivery to <span class="turquoise bl">##city## ##state##.</span></p>
                <p class="f14 grey-2">Your order is scheduled to arrive by  <span class="turquoise bl"><?php echo date('F j, Y', strtotime("+5 day")); ?></span></p>
            </div>

            <div id="form">
                <div id="form-top"></div>
                <div id="form-inner">
                    <form action="" method="POST" id="opt_in_form" name="opt_in_form" class="ng-pristine ng-valid" novalidate>
                        <img src="img/cards.jpg" alt="visa_master" id="cards" />
                        <!-- form elements -->
                </div>
                <div id="form-bottom">
                    <div class="form-button">
                        <div id="lock"></div>
                        <input value="COMPLETE CHECKOUT" onclick="isExit=false" type="submit" />
                    </div>
                </div>
                <input name="OPT" id="OPT" value="7a8f12a3-c940-4bf3-8f82-de1b0440a94c" type="hidden"><input name="form_token" id="form_token" value="5dd44d5329b66c07e0b52179a705ccfe30d51cea" type="hidden"><input name="campaign_id" id="campaign_id" value="617" type="hidden"><input name="shipping_id" id="shipping_id" value="30" type="hidden"><input name="shipping_id_price" id="shipping_id_price" value="4.95" type="hidden"><input name="product_id" id="product_id" value="292" type="hidden"><input name="product_qty" id="product_qty" value="1" type="hidden"><input name="product_price" id="product_price" value="0.00" type="hidden"><input name="insureship_product_id" id="insureship_product_id" value="" type="hidden"><input name="insureship_product_price" id="insureship_product_price" value="" type="hidden"><input name="insureship_product_qty" id="insureship_product_qty" value="" type="hidden"><input name="insureship_shipping_id" id="insureship_shipping_id" value="" type="hidden"><input name="insureship" id="insureship" value="0" type="hidden">
            </div>

            <div id="info">
                <div id="info-inner">
                    <div class="ng-scope" id="price-container" ng-controller="ListCtrl">
                        <img src="img/step1_400x400.png" alt="product" id="product" class="fl" />
                        <p id="insuredShipping">
                            <input type="checkbox" name="insuredShipping" checked="checked"> Yes, add <a href="#" id="insuredTooltip" class="tooltip">Protect Package<span class="tooltiptext"><span style="font-size: 160%;color:#336699;">Protect Package</span><br><br>Add guaranteed and expedited shipping to your order. If a shipment is lost or stolen, a new order will be immediately sent free of charge.</span></a> for $2.95 to my order.
                        </p>
                            </form>
                        <div id="price-info" class="fr ng-scope" ng-repeat="prodInfo in heading">

                            <div class="ng-scope" id="prod-info" ng-repeat="info in prodInfo.productContent">
                                <p class="bl ng-binding">##brandname##</p>
                                <p class="ng-binding">##productName##</p>
                                <p class="ng-binding">30ml - 30 Day Supply</p>
                            </div>
                            <div id="prices">
                                <div class="after bl bb">
                                    <p class="fr">$0.00</p>
                                    <p class="fl">Price</p>
                                </div>
                                <div class="after bb">
                                    <p class="fr">$##price##</p>
                                    <p class="fl">Shipping &amp; Handling:</p>
                                </div>
                                <div class="after">
                                    <p class="fr">$##price##</p>
                                    <p class="fl">TOTAL</p>
                                </div>
                                <img src="img/order/delivery.png" alt="delivery" id="delivery">
                            </div>
                        </div>
                    </div>
                    <img src="img/order/cp-arrow.png" alt="arrow" id="arrow">
                    <p></p>
                </div>
            </div>

        </div>
        <div id="terms" class="after ng-scope">
            <p class="ng-scope"></p>
        </div>
        <footer class="f11 after ng-scope"></footer>
    </div>
    <script src="assets/js/order.min.js"></script>
</body>
</html>
