$(function(){

    $('#viewing').delay(1700).animate({
      'opacity' : 1
    })

    $(window).on('load', function(){
         $('#reminder').delay(3350).animate({
          'bottom': -14
        });
        $('#reminder p').delay(3800).animate({
          'opacity': 1
        });
    });

    // $('.slide').on('click', function(){
    //     console.log('yes')
    // });

    var btn = $('#btn');
    var win = $(window);

    btn.stop().on('mouseover', function(){
      $('#btn img')
          .stop()
            .animate({
              top: 13
            }, 100);
    });
    btn.stop().on('mouseout', function(){
      $('#btn img')
          .stop()
            .animate({
              top: 15
            }, 100);
    });
    btn.on('click', function(){
      $('html, body').animate({
        scrollTop: $('#top').offset().top
      });
    });
    win.bind('scroll', function(){
        if(win.scrollTop() > 300){
          btn.stop().fadeIn(100);
        }else{
          btn.stop().fadeOut(100);
        }
    });

    var position1 = $('#top').offset();
    $(window).scroll(function (e) {
        var y = $(this).scrollTop();

        if(y >= position1.top + ($(this).height() * .45)){
            $('#section-1 .point-arrow').animate({
                'height' : '158px'
            }, 600);
        }

    });

    $(window).on("load", function(){
        $('#viewing').delay(1000).animate({
            'opacity' : 1
        });
    });
    // $('#upsell').on('click', function(){
    //   $('#preloader').css({'display' : 'block'});
    // });

      // var counter = 599;
      var one        = 0;
      var ten        = 0;
      var hundered   = 6;
      var intervalId = setInterval(function(){
                        time();
                      }, .7);
//COUNTER
      function time(){
         one--;

           if(one == -1){
              ten = ten - 1;
              one = 0 + 9;
            }
            if(ten == -1 ){
              hundered = hundered - 1;
              ten = 0 + 9;
            }
            var wholeNum = hundered+''+ten+''+one;
            if(wholeNum == 250){
              clearInterval(intervalId);
            }

         $('.timer').html('<span>'+hundered+'</span><span>'+ten+'</span><span>'+one+'</span>');

      }


//ANIMATE STEPS
      $('#slider').animate({
          'width' : '143px'
      }, 700);

      $('#ty-slider').animate({
          'width' : '295px'
      }, 700);

      $('#wrapper #banner #steps .step-2 div').css({
          'background' : '#01aac9'
      });

      $('#wrapper #banner #steps .thank-you div').css({
          'background' : '#01aac9'
      });

      $('.active > input').val();
      $('.active > input').focus();

    //   $('#popover').on('click', function(){
    //     $('#popover').css('display', 'none')
    //   });


});
