function startTimer(duration, display)
{
    var timer = duration, minutes, seconds;
    setInterval(function ()
    {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0)
        {
            timer = duration;
        }
    }, 1000);
}


window.onload = function ()
{
    var fiveMinutes = 60 * 5,
        display = document.querySelector('#countdown');
    startTimer(fiveMinutes, display);
};
function PopIt()
{
    var popover = document.getElementById('popover');

    popover.style.display = 'block';
    $('#popover').fadeIn();
    $('#pop-box').css({
        'top': 0,
        'opacity': 1,
        'transition-delay': '0s'
    })

    return "Are you sure you want to leave?";
}
function UnPopIt() { }

$(document).ready(function ()
{
    window.onbeforeunload = PopIt;
    $("a").click(function () { window.onbeforeunload = UnPopIt; });
});
$(function ()
{

    $('#viewing').delay(1700).animate({
        'opacity': 1
    })

    $(window).on('load', function ()
    {
        $('#reminder').delay(3350).animate({
            'bottom': -8
        });
        $('#reminder p').delay(3800).animate({
            'opacity': 1
        });
    });

    $('.slide').on('click', function ()
    {
        // $('html, body').animate({
        //   scrollTop : "0px"
        // })
        console.log('yes')
    });

    var btn = $('#btn');
    var win = $(window);

    btn.stop().on('mouseover', function ()
    {
        $('#btn img')
          .stop()
            .animate({
                top: 13
            }, 100);
    });
    btn.stop().on('mouseout', function ()
    {
        $('#btn img')
          .stop()
            .animate({
                top: 15
            }, 100);
    });
    btn.on('click', function ()
    {
        $('html, body').animate({
            scrollTop: $('#top').offset().top
        });
    });
    win.bind('scroll', function ()
    {
        if (win.scrollTop() > 300)
        {
            btn.stop().fadeIn(100);
        } else
        {
            btn.stop().fadeOut(100);
        }
    });

    var position1 = $('#top').offset();
    $(window).scroll(function (e)
    {
        var y = $(this).scrollTop();

        if (y >= position1.top + ($(this).height() * .45))
        {
            $('#section-1 .point-arrow').animate({
                'height': '158px'
            }, 600);
        }

        if (y >= $('#section-1').offset().top + ($(this).height() * .45))
        {
            $('#section-2 #sec-2-content #garcinia-points .point-arrow').animate({
                'height': '121px'
            }, 600);
            pumpingArrows();
        }
    });


    function pumpingArrows()
    {
        $('#section-2 #diagram-inner .left-arrow').animate({
            'left': '-40px'
        }, 500).animate({
            'left': '-30px'
        }, 400, pumpingArrows);

        $('#section-2 #diagram-inner .right-arrow').animate({
            'right': '-40px'
        }, 500).animate({
            'right': '-30px'
        }, 400, pumpingArrows);
    }


    $(window).on("load", function ()
    {
        $('#viewing').delay(1000).animate({
            'opacity': 1
        });
    });
    $('#upsell').on('click', function ()
    {
        $('#preloader').css({ 'display': 'block' });
    });

    // var counter = 599;
    var one = 0;
    var ten = 0;
    var hundered = 6;
    var intervalId = setInterval(function ()
    {
        time();
    }, .7);

    function time()
    {
        one--;

        if (one == -1)
        {
            ten = ten - 1;
            one = 0 + 9;
        }
        if (ten == -1)
        {
            hundered = hundered - 1;
            ten = 0 + 9;
        }
        var wholeNum = hundered + '' + ten + '' + one;
        if (wholeNum == 250)
        {
            clearInterval(intervalId);
        }

        $('.timer').html('<span>' + hundered + '</span><span>' + ten + '</span><span>' + one + '</span>');

    }

    var min = 0;
    var second = 00;
    var zeroPlaceholder = 0;
    var counterId = setInterval(function ()
    {
        countUp();
    }, 1000);

    function countUp()
    {
        second++;
        if (second == 59)
        {
            second = 00;
            min = min + 1;
        }
        if (second == 10)
        {
            zeroPlaceholder = '';
        } else
            if (second == 00)
            {
                zeroPlaceholder = 0;
            }

        $('.count-up').html(min + ':' + zeroPlaceholder + second);
    }

    $('#slider').animate({
        'width': '143px'
    }, 700);

    $('#ty-slider').animate({
        'width': '295px'
    }, 700);

    $('#wrapper #banner #steps .step-2 div').css({
        'background': '#01aac9'
    });

    $('#wrapper #banner #steps .thank-you div').css({
        'background': '#01aac9'
    });

    $('#pop-box a .close').on('click', function ()
    {
        $('#popover').css('display', 'none')
    });

    $('.active').val();
    $('.active').focus();

    $('#pop-box a .close').on('click', function ()
    {
        $('#popover').css('display', 'none')
    });

    $(':input[type="text"], select').on('focus', function ()
    {
        $(this).addClass('active');
    }).on('blur', function ()
    {
        $(this).removeClass('active');
    })


});


var stepOne = angular.module('stepOne', []);

stepOne.controller('ListCtrl', ['$scope', '$http',
  function ($scope, $http)
  {
      $http.get('content.json').success(function (data)
      {
          $scope.heading = data;
      });
  } ]);

stepOne.controller('headerDir', ['$scope', function ($scope)
{
    $scope.templates =
  [{ name: '../ngIncludes/content.html', url: 'ngIncludes/content.html' }
  ];
    $scope.template = $scope.templates[0];
} ]);

$.exitpop({
    predict_amt: 0.02,
    fps: 1000,
    popCount: 1,
    tolerance: { x: 10, y: 10 },
    cta: "#form",
    callback: function ()
    {
        $('#popover').fadeIn();
        $('#pop-box').css({
            'top': 0,
            'opacity': 1
        })
    }
})
