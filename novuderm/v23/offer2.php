
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" />
        <link rel="stylesheet" href="assets/css/offer2.min.css" />
    </head>
    <body>

        <div id="outer-wrapper" class="after">
            <div id="wrapper" class="after">
                <div id="banner">
                    <img src="https://images.templatemgr.com/brands/81/generic/step1/logo_400x200.png" height="80" width="80" alt="logo" class="logo" />
                    <div id="steps">
                        <ul>
                            <li class="step-1">
                                <div class="step">
                                    <p>1</p>
                                </div>
                                <p class="step-name">SHIPPING INFO</p>
                            </li>
                            <li class="step-2">
                                <div class="step">
                                    <p>2</p>
                                </div>
                                <p class="step-name" style="color:#01aac9;">FINISH ORDER</p>
                            </li>
                            <li class="step-3 thank-you">
                                <div class="step">
                                    <p>3</p>
                                </div>
                                <p class="step-name">SUMMARY</p>
                            </li>
                        </ul>
                        <div id="steps line">
                            <div id="ty-slider"></div>
                        </div>
                    </div>
                </div>

                <div id="thank-you">
                    <div>
                        <div class="tc">
                            <div id="wait">
                            </div>
                            <div id="upsell-btn" class="offer2_submit" onclick="offer2check();">
                                <img src="https://images.templatemgr.com/brands/81/generic/step1/step2_200x200.png" class="prod" alt="product" />
                                <div id="upsell-btn-content">
                                    <p><span class="f22">AMPLIFY YOUR RESULTS</span><br /><span class="f21">with <span class="bl">##brand_name##</span><br>##product_name##</span></p>
                                    <p><span class="f20">Add your <span class="bl turquoise">FREE TRIAL</span> bottle</span> <br /><span class="f18">Just pay shipping of
                                            <span class="bl">$##price##</span></span></p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div>
                                <img src="https://images.templatemgr.com/templates/v23/img/lock.jpg" alt="you can trust us" id="lock" />
                            </div>
                            <span id="upsell">
                                <span id="offer2_submit" onclick="offer2check();">
                                    <div id="up-btn">
                                        <div id="lock"></div>
                                        <p>COMPLETE CHECKOUT</p>
                                    </div>
                                    <div>
                                        <img src="https://images.templatemgr.com/templates/v23/img/security_logos.png" alt="we accept visa" id="cards" style="width:500px;height:auto;margin-top:20px;" />
                                    </div>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div id="decline">
                        <a href="" class="grey-2"><img src="https://images.templatemgr.com/templates/v23/img/x.jpg" alt="" /> No, Thanks. I decline the offer</a>
                    </div>
                    
                </div>
            </div>
        </div>
        <script src="assets/js/offer2.js"></script>
        <script src="assets/js/action.min.js"></script>
    </body>
</html>
