<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="css/foundation.min.css" />
        <link rel="stylesheet" href="css/index.min.css" />
    </head>
    <body>

        <div style="position: fixed; z-index: 6; top: 0px; left: 0px;"><img src="img/godaddy_secure.png"></div>
        <div style="background:#343434; position: fixed !important; width: 100%;top:0px; margin: 0px auto; z-index: 5;">
            <div style="color: #FFFFFF;font-size: 15px;margin: 0px auto;padding-left: 0;position: relative; text-align:center;padding:7px 0;">
                <span style="color:#ff0000;font-weight:bold;">WARNING:</span> Due to an extremely high media demand, there is a <strong>limited supply</strong> of Novuderm Collagen Serum in stock as of <span style="color:#ffff00">January 20 2014</span>. <strong>HURRY! </strong> <span style="position: fixed; margin-left: 4px;" id="time">10:00</span>
            </div>
        </div>
        <div id="wrapr" style="margin:30px 0 0 0;">
            <div id="topMain">
                <div id="topContent">
                    <form name="formID" id="opt_in_form" method="post" onsubmit="isExit=false;" novalidate>
                        <fieldset>
                            <!-- the form -->
                            <div class="row">
                                <div class="large-12 columns">
                                    <center>
                                        <input type="image" src="img/orderBtn.png" border="0" onmouseover="this.src='img/orderBtn_orange.png'" onmouseout="this.src='img/orderBtn.png'" name="btnorder_sub" id="btnorder_sub" class="btn" onclick="isExit=false;" style="width:100%;max-width:321px;margin-top: 20px;" />
                                    </center>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="topProduct"><img src="img/step1_400x400.png" height="240px" width="240px" alt="product" class="topProductIMG" /></div>
                <div class="topLogo"><img src="img/logo_400x200.png" height="120px" width="120px" alt="logo" class="topLogoIMG" /></div>
            </div>
            <a href="#" onclick="scrollToTop();">
                <div id="mainContent">
                    <div id="section1">
                        <div id="tabtitle">How Does Collagen Serum Work?</div>
                        <br /><br /><br /><br /><br /><br />
                        <div id="desc1"><strong>A combination of active ingredients work simultaneously to repair, restore and rejuvenate the skin's health and appearance from the inside out!</strong><br /><br />Novuderm Collagen Serum contains a proprietary formula of clinically validated ingredients that boost the skin's production of collagen, an elastin that assists in creating a protective barrier for the skin and provides a natural lift to the skin's dermal matrix at the cellular level. Novuderm Collagen Serum accelerates skin repair to rejuvenate luster and diminish fine lines and wrinkles, restoring the skin's youthful appearance.</div>
                    </div>
                    <div id="section2">
                        <div id="tabtitle">Benefits Of Collagen Serum</div>
                        <br /><br /><br /><br /><br /><br />
                        <div id="desc2">Novuderm Collagen Serum is a specially formulated emulsion of carefully selected ingredients that stimulate collagen and elastin production to deeply condition the skin and provide instant lifting and firming to smooth out fine lines and wrinkles. It contains antioxidants that help the skin combat free radical damage, keeping it looking healthy and youthful. Formulated with natural ingredients, Novuderm Collagen Serum is safe to use on all skin types.</div>
                        <div id="bottle2"><img src="img/step1_400x400.png" height="250px" width="250px" alt="product" /></div>
                    </div>
                    <div id="section3">
                        <div id="tabtitle">Clinically Proven Results</div>
                        <br /><br /><br /><br /><br /><br />
                        <div id="desc3top">Novuderm Collagen Serum consists of clinically validated ingredients that have been proven to assist with improved skin hydration, repair, and renewal. Clinical studies have shown that the proprietary formula targets the root cause of aging skin unlike many other products that only conceal them temporarily. Novuderm Collagen Serum will help you eliminate wrinkles, fade away fine lines, and reduce dark, while simultaneously improving skin tone and texture!</div>
                        <div id="desc3">Ever wondered how Hollywood celebrities defy age and look their best? Well you might think it's all makeup, but take a closer look. Even off the screen their age seems to have stood still! Reports indicate that their secret is a combination of natural anti-aging ingredients that helps them reduce and prevent visible signs of aging, making them look ageless. Some of the ingredients used by the celebrities are used in the Novuderm Collagen Serum formula.<br /><br /><br /><span class="purple">Now even you can radiate that glowing skin and oddles of confidence with Novuderm!<span></div>
                    </div>
                    <div id="logo4"><img src="img/logo_400x200.png" height="130px" width="130px" alt="logo" /></div>
                    <div id="section4">
                        <div id="tabtitle4">Real People. Real Results.</div>
                        <div id="desc4">I suffered from multiple skin problems including wrinkles, discoloration, and even inflammation. Since using Novuderm Collagen Serum, I have felt increased hydration and firmness, which has helped me fight the problem of expression lines and dark circles. Within just 4 weeks of use, I have noticed an improvement in my skin tone and texture! Highly recommended!<br /><br /><span class="right">- Janice V, 45</span></div>
                    </div>
                    <div id="section5">
                        <div id="logo5"><img src="img/logo_400x200.png" height="120px" width="120px" alt="logo" /></div>
                        <div id="bottle5"><img src="img/step1_400x400.png" height="260px" width="260px" alt="product" /></div>
                    </div>
                </div>
            </a>
        </div>

    </body>
</html>
