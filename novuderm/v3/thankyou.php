
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <link rel="stylesheet" href="css/foundation.min.css" />
        <link rel="stylesheet" href="css/thankyou.css" />

    </head>
    <body class="thanks">
        <div class="row">
            <div class="large-12 columns">
                <div align="center" class="contain">
                    <img src="img/thanksBg.png" class="headerIMG" />
                    <br>
                    <img src="img/logo_400x200.png" height="130px" width="130px" alt="logo" class="thanksLogoIMG" />
                    <br>
                </div>
                <div align="center" class="tybox">
                    <h1>Thank you for your order!</h1>
                    <h3>Product is in stock and will ship in the next 3-5 business days.</h3>
                    <br>
                    <br>
                    <h6 style="color:#000;">ORDER RECEIPT</h6>
                    <div class="box">
                        <div class="box_left box_ad_font">
                            <br />
                            <p><strong>Item:</strong> <?php echo product_prop("brand",1) . ' ' . product_prop("product_name",1) ?></p>
                            <p><strong>Order Placed:</strong> May 3 2009</p>
                            <p><strong>Delivery Estimate:</strong> May 10 2009 </p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>

            </div>
        </div>
    </body>
</html>
