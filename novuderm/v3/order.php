
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <link rel="stylesheet" href="css/foundation.min.css" />
        <link rel="stylesheet" href="css/order.min.css" />
    </head>
    <body>

        <div id="wrapr">
            <div style="width:1050px;height:790px;margin:0px auto;background:transparent url(img/order-2.png) top center no-repeat;" class="main-content">
                <img src="img/logo_400x200.png" height="130px" width="130px" alt="logo" class="orderLogoIMG" />
                <div style="width:615px;float:left;position:relative;top:147px;left:56px;" class="">
                    <div style="width:100%;padding:10px;color:#FFF;font-weight:bold;">
                        <span style="padding-right:420px;padding-left:20px;">PRODUCT</span>
                        <span>PRICE</span>
                    </div>
                    <div style="margin-top:30px;margin-left:10px;">
                        <img src="img/step1_400x400.png" height="240px" width="240px" alt="product" style="float:left;padding:10px 0px 10px 0px;width:200px;" />
                        <div id="info_box">
                            <span style="color:#8b53b7;font-size:18px;font-weight:bold;">Novuderm<br />Collagen Serum<br /></span><span style="font-size:12px;">30 Day Supply</span>
                            <br>
                            <span style="color:#F00;font-size:12px;line-height:2;">In Stock - Ships Within 24 Hours</span>
                            <p>&nbsp;</p>
                            <p><span style="font-size:20px;padding-right:200px;">&nbsp;Sub Total:</span><span style="font-size:20px;float:right;">$ 0.00</span><br></p>
                            <p style="background-color:#eee;"><span style="font-size:20px;padding-right:101px;">&nbsp;Shipping &amp; Handling:</span><span style="font-size:20px;float:right;">$0.00</span></p>
                            <p style="border-top:2px solid #8b53b7;color:#8b53b7;font-weight:bold;"><span style="font-size:20px;padding-right:220px;">&nbsp;TOTAL:</span><span style="font-size:20px;float:right;">$0.00</span></p>
                        </div>
                    </div>
                    <p id="order_info">Your order will arrive by <span style="color:#F00;"> March 20 2015</span>!</p>
                    <form method="post" name="opt_in_form" id="opt_in_form" novalidate>
                        <p id="insuredShipping">
                            <input type="checkbox" name="insuredShipping" checked="checked"> Yes, add <a href="#" id="insuredTooltip" class="tooltip">Protect Package<span class="tooltiptext"><span style="font-size: 160%;color:#336699;">Protect Package</span><br><br>Add guaranteed and expedited shipping to your order. If a shipment is lost or stolen, a new order will be immediately sent free of charge.</span></a> for $2.95 to my order.
                        </p>
                        <img src="img/order-1.png" id="order-arrow" />
                        
                </div>

                <div id="orderForm">
                    <center><img src="img/safeSecure.png" style="margin-top:107px;" /></center>
                    <div id="form_contain">
                        <!-- the form goes here -->
                        <center>
                            <input type="image" style="width:98%;max-width:305px;" class="btn" value="Submit" onclick="isExit=false;" id="form_button" name="btnorder_sub" onmouseout="this.src = 'img/orderBtn.png'" onmouseover="this.src = 'img/orderBtn_orange.png'" src="img/orderBtn.png">
                        </center>
                        <img src="img/form_b.png" alt="" id="form_btm" />
                    </div>
        </form>
                </div>
            </div>

        </div>
        <div id="footer">
            
        </div>
  </div>
    </body>
</html>
