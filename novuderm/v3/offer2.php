
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="css/main.min.css">
        <link rel="stylesheet" href="css/trial.min.css">
        <link rel="stylesheet" type="text/css" href="css/offer2.css" />
        <style>
            #ty-part1{background: url(https://images.templatemgr.com/templates/v3/img/Upsell-inner2.png) no-repeat center top;};
            
        </style>

    </head>
    <body>

        <div id="ty-section1">
            <div id="ty-content">
                <div id="ty-part1">
                    <p id="thanksmsg">Your order has been processed and your <strong>Novuderm<br />Collagen Serum</strong> will be shipped in the next 3-5 days!</p>
                    <p id="msg">For <span style="color:#dc2e25; font-weight:bold;">MAXIMUM</span> results, we highly recommend that you pair  <span style="color:#06b5b3;white-space: nowrap;"> Novuderm Collagen serum</span> with <span style="color:#c77471;white-space: nowrap;"> Novuderm Instant Lift! </span> Because you purchased  Collagen Serum, you're eligible to try  Instant Lift for just the cost of shipping and handling!</p>
                    <ul class="upsell_pnts thank_li" id="offer2points">
                        <li>Diminishes Wrinkles</li>
                        <br>
                        <li>Skin Repair &amp; Renewal</li>
                        <br>
                        <li>Prevents Skin Damage</li>
                        <br>
                        <li>Improves Skin Hydration</li>
                    </ul>
                    <img src="img/step1_400x400.png" id="offer2img">
                    <input type="submit" value="" id="offer2_submit" onclick="isExit=false;">
                </div>
            </div>
        </div>
        <div id="footer" style="margin-top: 1.7rem;margin-bottom: 8rem;">
            <div class="content-wrap">
                <div class="innercontainer" style="min-width: 1360px">
                    <p style="margin-top: -290px; font-family: arial;"><a href="">NO THANKS</a></p>
                    <br>
                </div>
            </div>
        </div>

    </body>
</html>
