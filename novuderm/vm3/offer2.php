<?php include_once($_SERVER['DOCUMENT_ROOT']."/settings/includes/offer2.php"); ?>
<!doctype html>
<html>
<head>
	<link rel="stylesheet" href="assets/css/main.min.css">
	<link rel="stylesheet" href="assets/css/trial.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/offer2.min.css">
	<style>
	<?php
		if (product_prop("price",2) == 4.95) {
			echo '#ty-part1{background: url(https://images.templatemgr.com/templates/v1/img/Upsell-inner222.png) no-repeat center top;}';
		}
		else {
			echo '#ty-part1{background: url(https://images.templatemgr.com/templates/v1/img/Upsell-inner111.png) no-repeat center top;}';
		}
	 ?>
	</style>
	<?php head();?>
</head>
<body>
	<?php body(); ?>
	<div id="ty-section1">
		<div id="ty-content">
			<div id="ty-part1">
				<p id="thanksmsg">Your order has been processed and your <strong><?php echo product_prop("brand",1) ?><br /><?php echo product_prop("product_name",1) ?></strong> will be shipped in the next 3-5 days!</p>
				<p id="msg">For <span style="color:#dc2e25; font-weight:bold;">MAXIMUM</span> results, we highly recommend that you pair  <span style="color:#06b5b3;white-space: nowrap;"> <?php echo product_prop("brand",1) . ' ' . product_prop("product_name",1) ?></span> with <span style="color:#c77471;white-space: nowrap;"> <?php echo product_prop("brand",2)." ".product_prop("product_name",2) ?>! </span> Because you purchased  <?php echo product_prop("product_name",1); ?>, you're eligible to try  <?php echo product_prop("product_name",2); ?> for just the cost of shipping and handling!</p>
				<ul class="upsell_pnts thank_li" id="offer2points">
					<li>Diminishes Wrinkles</li>
					<br>
					<li>Skin Repair &amp; Renewal</li>
					<br>
					<li>Prevents Skin Damage</li>
					<br>
					<li>Improves Skin Hydration</li>
				</ul>
				<img src="https://images.templatemgr.com/brands/<?php echo $_SESSION['brand_id']; ?>/generic/<?php echo product_prop("images_folder") ?>/step1_400x400.png"  id="offer2img">
				<input type="submit" value="" id ="offer2_submit" onClick="isExit=false;">
			</div>
		</div>
	</div>
	<div id="footer" style="margin-top: 1.7rem;margin-bottom: 8rem;">
		<div class="content-wrap">
			<div class="innercontainer" style="min-width: 1360px">
				<p style="margin-top: -290px; font-family: arial;"><a href="<?php echo $_SESSION['upsellNoUrl']; ?>" >NO THANKS</a></p>
				<br>
		</div>
	</div>
</div>
<?php footer(); ?>
</body>
</html>      
