<?php include_once($_SERVER['DOCUMENT_ROOT']."/settings/includes/order.php"); ?>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>  
  <link rel="stylesheet" href="assets/css/foundation.min.css" />
  <link rel="stylesheet" type="text/css" href="assets/css/order.min.css">
  <style type="text/css">
<?php if(isset($_SESSION['is_exit']) && $_SESSION['is_exit']==true): ?>
#footer {
  margin-top: 9rem;
}
#footer.goDown {
  margin-top: 26rem;
}
#fields_state {
  float: right !important;
}
#billing-info {
  margin-top: 80px !important;
}
#billingDiv {
  margin-left: -23px !important;
}
.input-validation {
  margin-right: -5px !important;
}
#shippingInfo {
  margin-top:12px;
}
#shippingInfo p {
  font-size: 12px !important;
}
.shippingrow {
  margin-top:-20px;
}
<?php endif ?>
  </style>
  <?php head(); ?>
</head>
<body>
  <?php body(); ?>
  <div id="wrapr" >
    <div style="width:1050px;height:790px;margin:0px auto;background:transparent url(https://images.templatemgr.com/templates/v3/img/order-2.png) top center no-repeat;" class="main-content">
      <img src="https://images.templatemgr.com/brands/<?php echo $_SESSION['brand_id']; ?>/generic/<?php echo product_prop("images_folder") ?>/logo_400x200.png" height="130px" width="130px" alt="logo" class="orderLogoIMG" />
      <div style="width:615px;float:left;position:relative;top:147px;left:56px;" class="">
        <div style="width:100%;padding:10px;color:#FFF;font-weight:bold;">
          <span style="padding-right:420px;padding-left:20px;">PRODUCT</span>
          <span>PRICE</span>
        </div>
        <div style="margin-top:30px;margin-left:10px;">
          <img src="https://images.templatemgr.com/brands/<?php echo $_SESSION['brand_id']; ?>/generic/<?php echo product_prop("images_folder") ?>/step1_400x400.png" height="240px" width="240px" alt="product" style="float:left;padding:10px 0px 10px 0px;width:200px;"/>
          <div id="info_box">
            <span style="color:#8b53b7;font-size:18px;font-weight:bold;"><?php echo product_prop("brand",1) ?><br /><?php echo product_prop("product_name",1) ?><br /></span><span style="font-size:12px;">30 Day Supply</span>
            <br>
            <span style="color:#F00;font-size:12px;line-height:2;">In Stock - Ships Within 24 Hours</span>
            <p>&nbsp;</p>
            <p><span style="font-size:20px;padding-right:200px;">&nbsp;Sub Total:</span><span style="font-size:20px;float:right;">$ 0.00</span><br></p>
            <p style="background-color:#eee;"><span style="font-size:20px;padding-right:101px;">&nbsp;Shipping &amp; Handling:</span><span style="font-size:20px;float:right;">$<?php if(isset($_SESSION['is_exit']) && $_SESSION['is_exit']==true) {echo '1.95';} else {echo product_prop("price",1);} ?></span></p>
            <p style="border-top:2px solid #8b53b7;color:#8b53b7;font-weight:bold;"><span style="font-size:20px;padding-right:220px;">&nbsp;TOTAL:</span><span style="font-size:20px;float:right;">$<?php if(isset($_SESSION['is_exit']) && $_SESSION['is_exit']==true) {echo '1.95';} else {echo product_prop("price",1);} ?></span></p>
          </div>
        </div>
        <p id="order_info">Your order will arrive by <span style="color:#F00;"> <?php echo date('F j, Y', strtotime("+5 day")); ?></span>!</p>
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" name='opt_in_form' id='opt_in_form' enctype="application/x-www-form-urlencoded;charset=ISO-8859-1" accept-charset="ISO-8859-1" novalidate>
          <p id="insuredShipping">
            <input type="checkbox" name="insuredShipping" checked="checked"> Yes, add <a href="#" id="insuredTooltip" class="tooltip">Protect Package<span class="tooltiptext"><span style="font-size: 160%;color:#336699;">Protect Package</span><br><br>Add guaranteed and expedited shipping to your order. If a shipment is lost or stolen, a new order will be immediately sent free of charge.</span></a> for $2.95 to my order.
          </p>
        <img src="https://images.templatemgr.com/templates/v3/img/order-1.png" id="order-arrow" />
        <!--<?php short_term(); ?>-->
      </div>

      <div id="orderForm">
          <center><img src="https://images.templatemgr.com/templates/v3/img/safeSecure.png" style="margin-top:107px;" /></center>
          <div id="form_contain">
            <?php the_form_insured(); ?>
            <center>
              <input type="image" style="width:98%;max-width:305px;" class="btn" value="Submit" onclick="isExit=false;"  id="form_button" name="btnorder_sub" onMouseOut="this.src = 'https://images.templatemgr.com/templates/v3/img/orderBtn.png'" onMouseOver="this.src = 'https://images.templatemgr.com/templates/v3/img/orderBtn_orange.png'" src="https://images.templatemgr.com/templates/v3/img/orderBtn.png">
            </center>
            <img src="https://images.templatemgr.com/templates/v3/img/form_b.png" alt="" id="form_btm"/>
          </div>
        </form>
      </div>
    </div>

    </div>
    <div id="footer">
    <?php footer(); ?>
  </div>
  </div>
</body>
</html>      
